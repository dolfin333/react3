import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import "./Header.css";
import { AppRoute, PicturePath } from "../../../common/enums/enums";
import { userActionCreator } from "./../../../store/actions";

const Header = () => {
  const { currentUser } = useSelector(({ user }) => {
    return {
      currentUser: user.currentUser,
    };
  });
  const dispatch = useDispatch();
  const history = useHistory();
  const handleLogout = useCallback(() => {
    dispatch(userActionCreator.logout());
  }, [dispatch]);

  const handleLogoutClicked = () => {
    handleLogout();
    history.push(AppRoute.LOGIN);
  };

  const handleUserListClicked = () => {
    history.push(AppRoute.USERS);
  };

  const handleChatClicked = () => {
    history.push(AppRoute.CHAT);
  };
  return (
    <div>
      {currentUser && (
        <div className="header">
          <div className="user-info">
            <div className="header-user-avatar">
              <img
                src={
                  currentUser.avatar
                    ? currentUser.avatar
                    : PicturePath.DEFAULT_USER
                }
                alt="avatar"
              />
            </div>
            <span className="user-name">{currentUser.name}</span>
          </div>
          <div className="routes-button">
            <div onClick={handleChatClicked} className="user-list-button">
              <img src={PicturePath.CHAT} alt="chat" />
            </div>
            {currentUser?.role === 1 && (
              <div onClick={handleUserListClicked} className="user-list-button">
                <img src={PicturePath.USER_LIST} alt="user list" />
              </div>
            )}
          </div>
          <div onClick={handleLogoutClicked} className="logout">
            <img src={PicturePath.LOGOUT} alt="logout" />
          </div>
        </div>
      )}
    </div>
  );
};

export default Header;
