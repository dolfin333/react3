import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import "./Chat.css";
import Preloader from "./preloader/Preloader";
import MessageInput from "./message_input/MessageInput";
import ChatHeader from "./chat_header/ChatHeader";
import MessageList from "./message_list/MessageList";
import { AppRoute } from "../../common/enums/enums";
import { chatActionCreator } from "../../store/actions";
import { KeyEditLastMessage } from "../../common/constants/constants";

const Chat = () => {
  const {
    messages,
    lastMessageDate,
    messageCounter,
    usersCounter,
    currentUser,
    preloader,
  } = useSelector(({ chat, user }) => {
    return {
      messages: chat.messages,
      preloader: chat.preloader,
      lastMessageDate: chat.lastMessageDate,
      messageCounter: chat.messageCounter,
      usersCounter: user.usersCounter,
      currentUser: user.currentUser,
    };
  });
  const dispatch = useDispatch();
  const history = useHistory();

  const handleSetUpdatingMessage = useCallback(
    (message) => {
      dispatch(chatActionCreator.setUpdatingMessage(message));
    },
    [dispatch]
  );

  const handleAddNewMessage = useCallback(
    (message) => {
      dispatch(chatActionCreator.createMessage(message));
    },
    [dispatch]
  );

  const handleDeleteMessage = useCallback(
    (id) => {
      dispatch(chatActionCreator.deleteMessage(id));
    },
    [dispatch]
  );

  const editMessage = (message) => {
    handleSetUpdatingMessage(message);
    history.push(AppRoute.CHAT + AppRoute.EDIT);
  };

  const getLastUserMessage = (messages, userId) => {
    const filteredMessages = messages.filter(
      (message) => message.userId === userId
    );
    if (filteredMessages.length !== 0) {
      return filteredMessages[filteredMessages.length - 1];
    }
    return null;
  };

  const handleKeyDown = (e) => {
    if (e.code === KeyEditLastMessage) {
      const lastMessage = getLastUserMessage(messages, currentUser?.id);
      if (lastMessage) {
        editMessage(lastMessage);
      }
    }
  };

  return (
    <div className="chat" tabIndex="1" onKeyDown={handleKeyDown}>
      {preloader ? (
        <Preloader />
      ) : (
        <>
          <div className="chat-main-content">
            <ChatHeader
              messageCounter={messageCounter}
              usersCounter={usersCounter}
              lastMessageDate={lastMessageDate}
            />
            <MessageList
              messages={messages}
              ownUserId={currentUser?.id}
              deleteMessage={handleDeleteMessage}
              editMessage={editMessage}
            />
          </div>
          <MessageInput
            onAddNewMessage={handleAddNewMessage}
            ownUser={currentUser}
          />
        </>
      )}
    </div>
  );
};

export default Chat;
