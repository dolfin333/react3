import React, { useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import "./MessageEdit.css";
import { AppRoute } from "../../../common/enums/enums";
import { chatActionCreator } from "../../../store/actions";

const MessageEdit = () => {
  const { updatingMessage } = useSelector(({ chat }) => {
    return {
      updatingMessage: chat.updatingMessage,
    };
  });

  const [text, setText] = useState(updatingMessage ? updatingMessage.text : "");
  const dispatch = useDispatch();
  const history = useHistory();

  const handleUpdateMessage = useCallback(
    (message) => {
      dispatch(chatActionCreator.updateMessage(message));
    },
    [dispatch]
  );

  const handleCancel = useCallback(
    (message) => {
      dispatch(chatActionCreator.setUpdatingMessage(message));
    },
    [dispatch]
  );

  const onTextChange = (e) => {
    setText(e.target.value);
  };

  const onCancel = () => {
    handleCancel(null);
    history.push(AppRoute.CHAT);
  };
  const onEdit = () => {
    if (text !== "") {
      if (text !== updatingMessage.text) {
        handleUpdateMessage({
          ...updatingMessage,
          text,
          editedAt: new Date(),
        });
      }
      history.push(AppRoute.CHAT);
    }
  };
  return (
    <div>
      <div className="message-edit-container">
        <textarea
          className="edit-message-textarea"
          value={text}
          onChange={onTextChange}
        />
        <div className="message-edit-buttons">
          <button className="btn" onClick={onCancel}>
            Cancel
          </button>
          <button className="btn" onClick={onEdit}>
            Edit
          </button>
        </div>
      </div>
    </div>
  );
};

export default MessageEdit;
