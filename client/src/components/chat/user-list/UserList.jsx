import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import "./UserList.css";
import Preloader from "../preloader/Preloader";
import { AppRoute } from "../../../common/enums/enums";
import User from "./../user/User";

const UserList = () => {
  const { users, preloader, currentUser } = useSelector(({ user }) => {
    return {
      users: user.users,
      currentUser: user.currentUser,
      preloader: user.preloader,
    };
  });
  const history = useHistory();

  const onAdd = () => {
    history.push(AppRoute.USERS + AppRoute.EDITOR);
  };
  return (
    <>
      {preloader ? (
        <Preloader />
      ) : (
        <div className="user-list-container">
          <button className="btn" onClick={onAdd}>
            Add
          </button>
          <div className="user-list-wrapper">
            <div className="user-list">
              {users.map((user) => (
                <User user={user} currentUser={currentUser} key={user.id} />
              ))}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default UserList;
