import React, { useState } from "react";
import moment from "moment";
import { PicturePath } from "../../../common/enums/enums";
import "./Message.css";

const Message = ({ message }) => {
  const [isLiked, setIsLiked] = useState(false);
  const getFormatedDate = (message) => {
    const dateOb = message.editedAt
      ? new Date(message.editedAt)
      : new Date(message.createdAt);
    return moment(dateOb).format("HH:mm");
  };

  const onLike = () => {
    setIsLiked(!isLiked);
  };

  return (
    <div className="message">
      <div className="message-user-avatar">
        <img
          src={message.avatar ? message.avatar : PicturePath.DEFAULT_USER}
          alt="avatar"
        />
      </div>
      <div className="main-mess-info">
        <div className="message-header">
          <div className="message-user-name">{message.user}</div>
          <button
            className={isLiked ? "message-liked" : "message-like"}
            onClick={onLike}
          >
            {isLiked ? (
              <img src={PicturePath.FULL_HEART} alt="heart" />
            ) : (
              <img src={PicturePath.EMPTY_HEART} alt="heart" />
            )}
          </button>
        </div>
        <div className="message-text">{message.text}</div>
        <div className="message-time">
          {message.editedAt && "edited at "}
          {getFormatedDate(message)}
        </div>
      </div>
    </div>
  );
};

export default Message;
