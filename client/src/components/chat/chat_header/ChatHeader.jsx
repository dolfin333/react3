import moment from "moment";
import React from "react";
import { DateFormats } from "../../../common/enums/enums";
import "./ChatHeader.css";

const ChatHeader = ({ usersCounter, messageCounter, lastMessageDate }) => {
  const getDateFormat = (lastMessageDate) => {
    if (lastMessageDate !== "") {
      return moment(lastMessageDate).format(DateFormats.DD_MM_YYYY_HH_mm);
    }
    return "";
  };
  return (
    <div className="chat-header">
      <div className="header-left-content">
        <div className="header-title">Sweety chat</div>
        <div className="header-users-count-wrap">
          <span>Users</span>
          <div className="header-users-count">{usersCounter}</div>
        </div>
        <div className="header-messages-count-wrap">
          <span>Messages</span>
          <div className="header-messages-count">{messageCounter}</div>
        </div>
      </div>
      <div className="header-last-message-date-wrap">
        <span>Last message</span>
        <div className="header-last-message-date">
          {getDateFormat(lastMessageDate)}
        </div>
      </div>
    </div>
  );
};

export default ChatHeader;
