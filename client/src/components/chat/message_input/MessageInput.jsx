import React, { useState } from "react";
import "./MessageInput.css";

const MessageInput = ({ onAddNewMessage, ownUser }) => {
  const handleAddNewMessage = () => {
    if (inputText !== "") {
      setInputText("");
      onAddNewMessage({
        text: inputText,
        userId: ownUser.id,
        user: ownUser.name,
        avatar: ownUser.avatar,
      });
    }
  };

  const [inputText, setInputText] = useState("");
  const onTextChange = (e) => {
    setInputText(e.target.value);
  };
  return (
    <div className="message-input">
      <div className="message-input-content">
        <input
          value={inputText}
          className="message-input-text"
          onChange={onTextChange}
        />
        <button className="message-input-button" onClick={handleAddNewMessage}>
          Send
        </button>
      </div>
    </div>
  );
};

export default MessageInput;
