import React, { useCallback } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import "./User.css";
import { AppRoute } from "../../../common/enums/enums";
import { userActionCreator } from "../../../store/actions";

const User = ({ user, currentUser }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const handleSetUpdatingUser = useCallback(
    (user) => {
      dispatch(userActionCreator.setUpdatingUser(user));
    },
    [dispatch]
  );

  const handleDeleteUser = useCallback(
    (userId) => {
      dispatch(userActionCreator.deleteUser(userId));
    },
    [dispatch]
  );

  const editUser = () => {
    handleSetUpdatingUser(user);
    history.push(AppRoute.USERS + AppRoute.EDITOR);
  };
  const onDeleteClicked = () => {
    handleDeleteUser(user.id);
  };

  return (
    <div className="user">
      <div>
        {user.name} | {user.login}
      </div>
      <div className="buttons">
        <button className="btn" onClick={editUser}>
          Edit
        </button>
        {user.id != currentUser?.id && (
          <button className="btn" onClick={onDeleteClicked}>
            Delete
          </button>
        )}
      </div>
    </div>
  );
};

export default User;
