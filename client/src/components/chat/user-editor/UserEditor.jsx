import React, { useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { AppRoute, UserPayloadKey } from "../../../common/enums/enums";
import { userActionCreator } from "./../../../store/actions";
import "./UserEditor.css";

const UserEditor = () => {
  const { updatingUser } = useSelector(({ user }) => {
    return {
      updatingUser: user.updatingUser,
    };
  });
  const [userData, setUserData] = useState(
    updatingUser
      ? { login: updatingUser.login, name: updatingUser.name }
      : { login: "", name: "", password: "" }
  );
  const dispatch = useDispatch();
  const history = useHistory();
  const removeUpdatingUser = useCallback(
    (user) => {
      dispatch(userActionCreator.setUpdatingUser(user));
    },
    [dispatch]
  );

  const handleUpdateUser = useCallback(
    (user) => {
      dispatch(userActionCreator.updateUser(user));
    },
    [dispatch]
  );

  const handleAddUser = useCallback(
    (user) => {
      dispatch(userActionCreator.createUser(user));
    },
    [dispatch]
  );

  const handleChange = (name, value) => {
    setUserData({
      ...userData,
      [name]: value,
    });
  };

  const onLoginChange = (e) => {
    handleChange(UserPayloadKey.LOGIN, e.target.value);
  };
  const onNameChange = (e) => {
    handleChange(UserPayloadKey.NAME, e.target.value);
  };
  const onPasswordChange = (e) => {
    handleChange(UserPayloadKey.PASSWORD, e.target.value);
  };

  const onCancel = () => {
    removeUpdatingUser(null);
    history.push(AppRoute.USERS);
  };
  const onEdit = () => {
    handleUpdateUser({ ...updatingUser, ...userData });
    removeUpdatingUser(null);
    history.push(AppRoute.USERS);
  };
  const onAdd = () => {
    handleAddUser({ ...userData });
    history.push(AppRoute.USERS);
  };
  return (
    <div className="user-editor-container">
      <div className="user-editor">
        <div className="input-title">Login</div>
        <input
          type="text"
          className="input"
          value={userData.login}
          onChange={onLoginChange}
        />
        <div className="input-title">Name</div>
        <input
          type="text"
          className="input"
          value={userData.name}
          onChange={onNameChange}
        />
        {!updatingUser && (
          <>
            <div className="input-title">Password</div>
            <input
              type="password"
              className="input"
              value={userData.password}
              onChange={onPasswordChange}
            />
          </>
        )}
        <div className="user-editor-buttons">
          <button className="btn" onClick={onCancel}>
            Cancel
          </button>
          {updatingUser ? (
            <button className="btn" onClick={onEdit}>
              Edit
            </button>
          ) : (
            <button className="btn" onClick={onAdd}>
              Add
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default UserEditor;
