import React from "react";
import moment from "moment";
import { DateFormats } from "../../../common/enums/enums";
import "./OwnMessage.css";

const OwnMessage = ({ editMessage, message, deleteMessage }) => {
  const getFormatedDate = (message) => {
    const dateOb = message.editedAt
      ? new Date(message.editedAt)
      : new Date(message.createdAt);
    return moment(dateOb).format(DateFormats.HH_mm);
  };

  return (
    <div className="own-message-wrap">
      <div className="own-message">
        <div className="buttons">
          <button
            className="message-edit"
            onClick={() => {
              editMessage(message);
            }}
          >
            edit
          </button>
          <button
            className="message-delete"
            onClick={() => {
              deleteMessage(message.id);
            }}
          >
            delete
          </button>
        </div>
        <div className="own-message-content">
          <div className="message-text">{message.text}</div>
          <div className="message-time">
            {message.editedAt && "edited at "}
            {getFormatedDate(message)}
          </div>
        </div>
      </div>
    </div>
  );
};

export default OwnMessage;
