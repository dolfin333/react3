import React from "react";
import moment from "moment";
import Message from "../message/Message";
import OwnMessage from "../own_message/OwnMessage";
import "./MessageList.css";
import { DateDays, DateFormats } from "../../../common/enums/enums";

const MessageList = ({ messages, ownUserId, deleteMessage, editMessage }) => {
  const groupByDay = (array) => {
    const mapValuesByDate = new Map();
    const copyArray = array.slice(0);
    copyArray.forEach((value) => {
      let d = moment(new Date(value.createdAt)).format(DateFormats.l);
      if (typeof mapValuesByDate.get(d) !== "undefined") {
        mapValuesByDate.get(d).push(value);
      } else {
        mapValuesByDate.set(d, [value]);
      }
    });
    return mapValuesByDate;
  };

  const getDifferenceInDays = (date1, date2) => {
    const days = Math.floor(
      (date2.getTime() - date1.getTime()) / (1000 * 3600 * 24)
    );
    return days;
  };
  const getDate = (date1, date2) => {
    const diffDays = getDifferenceInDays(date1, date2);
    if (diffDays === 0) {
      return DateDays.TODAY;
    }
    if (diffDays === 1) {
      return DateDays.YESTERDAY;
    }
    return moment(date1).format(DateFormats.dddd_DD_MMMM);
  };

  const messagesMap = groupByDay(messages);

  return (
    <div className="message-list">
      {Array.from(messagesMap.entries()).map(([key, value]) => (
        <div key={value[0].id + value[0].userId}>
          <div className="messages-divider">
            <div className="messages-divider-date">
              {getDate(new Date(key), new Date())}
            </div>
          </div>
          {value.map((message) =>
            message.userId === ownUserId ? (
              <OwnMessage
                message={message}
                key={message.id}
                deleteMessage={deleteMessage}
                editMessage={editMessage}
              />
            ) : (
              <Message message={message} key={message.id} />
            )
          )}
        </div>
      ))}
    </div>
  );
};

export default MessageList;
