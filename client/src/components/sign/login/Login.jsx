import React, { useState, useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import "./Login.css";
import { userActionCreator, chatActionCreator } from "./../../../store/actions";
import { AppRoute, UserPayloadKey } from "../../../common/enums/enums";

const Login = () => {
  const { currentUser, error } = useSelector(({ user }) => {
    return { currentUser: user.currentUser, error: user.error };
  });
  const [userData, setUserData] = useState({ login: "", password: "" });
  const [errorMess, setErrorMess] = useState(error);
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(() => {
    setErrorMess(error);
    if (currentUser) {
      dispatch(userActionCreator.loadUsers());
      dispatch(chatActionCreator.loadMessages());
      if (currentUser.role === 1) {
        history.push(AppRoute.USERS);
      } else {
        history.push(AppRoute.CHAT);
      }
    }
  }, [dispatch, history, currentUser, error]);

  const handleLogin = useCallback(
    (userData) => {
      return dispatch(userActionCreator.login(userData));
    },
    [dispatch]
  );

  const handleLoginClicked = () => {
    handleLogin(userData).then(() => {
      if (error) {
        setErrorMess(error);
      }
    });
  };

  const handleChange = (name, value) => {
    setErrorMess("");
    setUserData({
      ...userData,
      [name]: value,
    });
  };

  const onLoginChange = (e) => {
    handleChange(UserPayloadKey.LOGIN, e.target.value);
  };
  const onPasswordChange = (e) => {
    handleChange(UserPayloadKey.PASSWORD, e.target.value);
  };

  return (
    <div className="login">
      <div className="login-container">
        <div className="error">{errorMess}</div>
        <div className="input-title">Login</div>
        <input
          type="text"
          className="input"
          value={userData.login}
          onChange={onLoginChange}
        />
        <div className="input-title">Password</div>
        <input
          type="password"
          className="password input"
          value={userData.password}
          onChange={onPasswordChange}
        />
        <button onClick={handleLoginClicked} className="btn">
          Login
        </button>
      </div>
    </div>
  );
};

export default Login;
