const AppRoute = {
  ROOT: "/",
  ANY: "*",
  LOGIN: "/login",
  CHAT: "/chat",
  EDIT: "/edit",
  EDITOR: "/editor",
  USERS: "/users",
};

export { AppRoute };
