const PicturePath = {
  CHAT: "/chat.png",
  USER_LIST: "/user-list.png",
  LOGOUT: "/logout.svg.png",
  DEFAULT_USER: "/default-user.webp",
  FULL_HEART: "/full-heart.webp",
  EMPTY_HEART: "/empty-heart.png",
};

export { PicturePath };
