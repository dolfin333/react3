const DateFormats = {
  dddd_DD_MMMM: "dddd, DD MMMM",
  l: "l",
  DD_MM_YYYY_HH_mm: "DD.MM.YYYY HH:mm",
  HH_mm: "HH:mm",
};

export { DateFormats };
