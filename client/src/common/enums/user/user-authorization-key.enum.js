const UserPayloadKey = {
  PASSWORD: "password",
  LOGIN: "login",
  NAME: "name",
};

export { UserPayloadKey };
