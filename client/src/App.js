import { Switch, Route, useHistory } from "react-router-dom";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import "./App.css";
import Chat from "./components/chat/Chat";
import MessageEdit from "./components/chat/message_edit/MessageEdit";
import Login from "./components/sign/login/Login";
import UserList from "./components/chat/user-list/UserList";
import UserEditor from "./components/chat/user-editor/UserEditor";
import Header from "./components/common/header/Header";
import {
  storage as storageService,
  users as userService,
} from "./services/services";
import { StorageKey, AppRoute } from "./common/enums/enums";
import { userActionCreator, chatActionCreator } from "./store/actions";

function App() {
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(() => {
    const userId = storageService.getItem(StorageKey.TOKEN);
    if (userId) {
      userService.getUser(userId).then((user) => {
        dispatch(userActionCreator.setCurrentUser(user));
        dispatch(userActionCreator.loadUsers());
        dispatch(chatActionCreator.loadMessages());
        if (user.role === 1) {
          history.push(AppRoute.USERS);
        } else {
          history.push(AppRoute.CHAT);
        }
      });
    } else {
      history.push(AppRoute.LOGIN);
    }
  }, [dispatch, history]);

  return (
    <div className="App">
      <div className="container">
        <Header />
        <Switch>
          <Route path="/chat" exact>
            <Chat />
          </Route>
          <Route path="/chat/edit" exact>
            <MessageEdit />
          </Route>
          <Route path="/login" exact>
            <Login />
          </Route>
          <Route path="/users" exact>
            <UserList />
          </Route>
          <Route path="/users/editor" exact>
            <UserEditor />
          </Route>
        </Switch>
      </div>
    </div>
  );
}

export default App;
