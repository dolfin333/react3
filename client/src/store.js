import { configureStore } from "@reduxjs/toolkit";
import { chatReducer, userReducer } from "./store/reducers";
import { enableMapSet } from "immer";

enableMapSet();

const store = configureStore({
  reducer: {
    chat: chatReducer,
    user: userReducer,
  },
});

export default store;
