import { HttpMethod, ContentType } from "../../common/enums/enums";

class User {
  constructor({ http }) {
    this._http = http;
  }

  getAllUsers(filter) {
    return this._http.load("/api/users", {
      method: HttpMethod.GET,
    });
  }

  getUser(id) {
    return this._http.load(`/api/users/${id}`, {
      method: HttpMethod.GET,
    });
  }

  getUserAuth(payload) {
    return this._http.load(`/api/users/auth`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  addUser(payload) {
    return this._http.load("/api/users", {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  updateUser(payload, id) {
    return this._http.load(`/api/users/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  deleteUser(id) {
    return this._http.load(`/api/users/${id}`, {
      method: HttpMethod.DELETE,
    });
  }
}

export default User;
