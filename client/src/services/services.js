import Message from "./message/message.service.js";
import User from "./user/user.service.js";
import { Http } from "./http/http.service";
import { Storage } from "./storage/storage.service";

const storage = new Storage({
  storage: localStorage,
});

const http = new Http({
  storage,
});

const messages = new Message({
  http,
});

const users = new User({
  http,
});

export { http, storage, messages, users };
