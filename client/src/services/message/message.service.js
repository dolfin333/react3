import { HttpMethod, ContentType } from "../../common/enums/enums";

class Message {
  constructor({ http }) {
    this._http = http;
  }

  getMessages() {
    return this._http.load("/api/messages", {
      method: HttpMethod.GET,
    });
  }

  getMessage(id) {
    return this._http.load(`/api/messages/${id}`, {
      method: HttpMethod.GET,
    });
  }

  addMessage(payload) {
    return this._http.load("/api/messages", {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  updateMessage(payload, id) {
    return this._http.load(`/api/messages/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  deleteMessage(id) {
    return this._http.load(`/api/messages/${id}`, {
      method: HttpMethod.DELETE,
    });
  }
}

export default Message;
