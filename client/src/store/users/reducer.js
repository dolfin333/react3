import { createReducer } from "@reduxjs/toolkit";
import {
  SET_CURRENT_USER,
  SET_USERS,
  SET_UPDATING_USER,
  ADD_USER,
  SET_PRELOADER,
  SET_ERROR,
} from "./actionTypes";

const initialState = {
  usersCounter: null,
  users: [],
  currentUser: null,
  updatingUser: null,
  preloader: false,
  error: "",
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(ADD_USER, (state, { payload }) => {
    const { user } = payload;
    state.users = [...state.users, user];
    state.usersCounter += 1;
  });
  builder.addCase(SET_USERS, (state, { payload }) => {
    const { users } = payload;
    state.users = users;
    state.usersCounter = users.length;
  });
  builder.addCase(SET_UPDATING_USER, (state, { payload }) => {
    const { user } = payload;
    state.updatingUser = user;
  });
  builder.addCase(SET_CURRENT_USER, (state, { payload }) => {
    const { user } = payload;
    state.currentUser = user;
  });
  builder.addCase(SET_PRELOADER, (state, { payload }) => {
    const { preloader } = payload;
    state.preloader = preloader;
  });
  builder.addCase(SET_ERROR, (state, { payload }) => {
    const { error } = payload;
    state.error = error;
  });
});

export { reducer };
