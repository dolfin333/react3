import { createAction } from "@reduxjs/toolkit";
import { Error, StorageKey } from "../../common/enums/enums";
import { setMessages } from "../chat/actions";
import {
  storage as storageService,
  users as userService,
} from "./../../services/services";
import {
  SET_USERS,
  ADD_USER,
  SET_UPDATING_USER,
  SET_CURRENT_USER,
  SET_PRELOADER,
  SET_ERROR,
} from "./actionTypes";

const setPreloader = createAction(SET_PRELOADER, (preloader) => ({
  payload: {
    preloader,
  },
}));

const setUpdatingUser = createAction(SET_UPDATING_USER, (user) => ({
  payload: {
    user,
  },
}));

const setCurrentUser = createAction(SET_CURRENT_USER, (user) => ({
  payload: {
    user,
  },
}));

const setUsers = createAction(SET_USERS, (users) => ({
  payload: {
    users,
  },
}));

const addUser = createAction(ADD_USER, (user) => ({
  payload: {
    user,
  },
}));

const setError = createAction(SET_ERROR, (error) => ({
  payload: {
    error,
  },
}));

const login =
  ({ login, password }) =>
  async (dispatch) => {
    try {
      const user = await userService.getUserAuth({ login, password });
      storageService.setItem(StorageKey.TOKEN, user.id);
      dispatch(setCurrentUser(user));
      dispatch(setError(""));
    } catch {
      dispatch(setError(Error.NO_SUCH_USER));
    }
  };

const logout = () => async (dispatch) => {
  storageService.removeItem(StorageKey.TOKEN);
  dispatch(setCurrentUser(null));
};

const loadUsers = () => async (dispatch) => {
  dispatch(setPreloader(true));
  const users = await userService.getAllUsers();
  dispatch(setUsers(users));
  dispatch(setPreloader(false));
};

const createUser = (user) => async (dispatch) => {
  const { id } = await userService.addUser(user);
  const newUser = await userService.getUser(id);
  dispatch(addUser(newUser));
};

const deleteUser = (userId) => async (dispatch, getRootState) => {
  await userService.deleteUser(userId);
  const {
    user: { users },
  } = getRootState();
  const updatedUsers = users.filter((user) => user.id !== userId);
  dispatch(setUsers(updatedUsers));
};

const updateUser = (user) => async (dispatch, getRootState) => {
  const newUser = await userService.updateUser(user, user.id);
  const {
    user: { users, currentUser },
    chat: { messages },
  } = getRootState();
  const messagesCopy = [...messages];
  const updatedMessages = messagesCopy.map((message) => {
    if (message.userId === user.id) {
      return { ...message, user: user.name };
    }
    return message;
  });
  const usersCopy = [...users];
  const indexUser = usersCopy.findIndex(
    (stateUser) => stateUser.id === newUser.id
  );
  usersCopy[indexUser] = newUser;
  if (currentUser.id === newUser.id) {
    dispatch(setCurrentUser(newUser));
  }
  dispatch(setUsers(usersCopy));
  dispatch(setMessages(updatedMessages));
};

export {
  setCurrentUser,
  setUpdatingUser,
  login,
  logout,
  updateUser,
  loadUsers,
  deleteUser,
  createUser,
};
