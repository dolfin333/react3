import { createReducer } from "@reduxjs/toolkit";
import {
  SET_UPDATING_MESSAGE,
  ADD_MESSAGE,
  SET_MESSAGES,
  SET_PRELOADER,
} from "./actionTypes";

const initialState = {
  messages: [],
  preloader: false,
  lastMessageDate: null,
  messageCounter: null,
  usersCounter: null,
  updatingMessage: null,
  status: null,
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(ADD_MESSAGE, (state, { payload }) => {
    const { message } = payload;
    state.messages = [...state.messages, message];
    state.messageCounter = state.messages.length;
    state.lastMessageDate = message.createdAt;
  });
  builder.addCase(SET_MESSAGES, (state, { payload }) => {
    const { messages } = payload;
    messages.sort((a, b) => {
      return new Date(a.createdAt) - new Date(b.createdAt);
    });
    state.messages = messages;
    state.messageCounter = messages.length;
    state.lastMessageDate = messages[messages.length - 1]
      ? messages[messages.length - 1].createdAt
      : "";
  });
  builder.addCase(SET_UPDATING_MESSAGE, (state, { payload }) => {
    const { message } = payload;
    state.updatingMessage = message;
  });
  builder.addCase(SET_PRELOADER, (state, { payload }) => {
    const { preloader } = payload;
    state.preloader = preloader;
  });
});

export { reducer };
