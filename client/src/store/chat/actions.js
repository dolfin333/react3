import { createAction } from "@reduxjs/toolkit";
import { messages as messageService } from "./../../services/services";
import {
  ADD_MESSAGE,
  SET_MESSAGES,
  SET_UPDATING_MESSAGE,
  SET_PRELOADER,
} from "./actionTypes";

const setPreloader = createAction(SET_PRELOADER, (preloader) => ({
  payload: {
    preloader,
  },
}));

const setUpdatingMessage = createAction(SET_UPDATING_MESSAGE, (message) => ({
  payload: {
    message,
  },
}));

const setMessages = createAction(SET_MESSAGES, (messages) => ({
  payload: {
    messages,
  },
}));

const addMessage = createAction(ADD_MESSAGE, (message) => ({
  payload: {
    message,
  },
}));

const loadMessages = () => async (dispatch) => {
  dispatch(setPreloader(true));
  const messages = await messageService.getMessages();
  dispatch(setMessages(messages));
  dispatch(setPreloader(false));
};

const createMessage = (message) => async (dispatch) => {
  const { id } = await messageService.addMessage(message);
  const newMessage = await messageService.getMessage(id);
  dispatch(addMessage(newMessage));
};

const deleteMessage = (messageId) => async (dispatch, getRootState) => {
  await messageService.deleteMessage(messageId);
  const {
    chat: { messages },
  } = getRootState();
  const updatedMessages = messages.filter(
    (message) => message.id !== messageId
  );
  dispatch(setMessages(updatedMessages));
};

const updateMessage = (message) => async (dispatch, getRootState) => {
  const newMessage = await messageService.updateMessage(message, message.id);
  const {
    chat: { messages },
  } = getRootState();
  const messagesCopy = [...messages];
  const indexMess = messagesCopy.findIndex(
    (stateMessage) => stateMessage.id === message.id
  );
  messagesCopy[indexMess] = newMessage;
  dispatch(setMessages(messagesCopy));
};

export {
  addMessage,
  setMessages,
  setUpdatingMessage,
  loadMessages,
  createMessage,
  deleteMessage,
  updateMessage,
};
