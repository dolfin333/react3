import initApi from "./api/api.js";
import express from "express";
const server = express();
import cors from "cors";

server.use(cors());
server.use(express.json());

server.use("/api", initApi(express.Router));
server.listen(3001, function () {
  console.log("Server is ready");
});
