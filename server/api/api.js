import UserService from "../services/user/user.service.js";
import initUser from "./user/user.api.js";
import MessageService from "../services/message/message.service.js";
import initMessage from "./message/message.api.js";
import path from "path";
import { fileURLToPath } from "url";
const __dirname = path.dirname(fileURLToPath(import.meta.url));

// register all routes
const initApi = (Router) => {
  const apiRouter = Router();
  apiRouter.use(
    "/messages",
    initMessage(Router, {
      MessageService: new MessageService(
        path.resolve(__dirname, "../data/messages.json")
      ),
    })
  );
  apiRouter.use(
    "/users",
    initUser(Router, {
      UserService: new UserService(
        path.resolve(__dirname, "../data/users.json")
      ),
      MessageService: new MessageService(
        path.resolve(__dirname, "../data/messages.json")
      ),
    })
  );

  return apiRouter;
};

export default initApi;
