const initUser = (Router, services) => {
  const { UserService, MessageService } = services;
  const router = Router();
  router
    .post("/auth", (req, res, next) =>
      res.send(UserService.getUserByLoginAndPassword(req.body))
    )
    .get("/", (req, res, next) => res.send(UserService.getUsers()))
    .get("/:id", (req, res, next) =>
      res.send(UserService.getUserById(req.params.id))
    )
    .post("/", (req, res, next) => res.send(UserService.addUser(req.body)))
    .put("/:id", (req, res, next) => {
      const newUser = UserService.updateUser(req.params.id, req.body);
      MessageService.updateMessagesUser(newUser);
      res.send(newUser);
    })
    .delete("/:id", (req, res, next) =>
      res.send(UserService.deleteUser(req.params.id))
    );

  return router;
};
export default initUser;
