const initMessage = (Router, services) => {
  const { MessageService } = services;
  const router = Router();
  router
    .get("/", (req, res, next) => res.send(MessageService.getMessages()))
    .get("/:id", (req, res, next) =>
      res.send(MessageService.getMessageById(req.params.id))
    )
    .post("/", (req, res, next) =>
      res.send(MessageService.addMessage(req.body))
    )
    .put("/:id", (req, res, next) =>
      res.send(MessageService.updateMessage(req.params.id, req.body))
    )
    .delete("/:id", (req, res, next) =>
      res.send(MessageService.deleteMessage(req.params.id))
    );

  return router;
};
export default initMessage;
