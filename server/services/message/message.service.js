import JsonService from "../json.service.js";

class MessageService {
  constructor(filePath) {
    this.storage = new JsonService(filePath);
  }

  getMessages() {
    const messages = this.storage.readItems();
    return messages;
  }

  getMessageById(id) {
    const messages = this.storage.readItems();
    const message = messages.find((message) => message.id == id);
    return message;
  }

  addMessage(message) {
    const newMessage = {
      createdAt: new Date(),
      editedAt: "",
      ...message,
    };
    const messages = this.storage.readItems();
    newMessage.id = this.storage.nextId.toString();
    messages.push(newMessage);
    this.storage.writeItems(messages);
    this.storage.incrementNextId();
    return newMessage;
  }

  updateMessage(id, message) {
    const messages = this.storage.readItems();
    const messageIndex = messages.findIndex((message) => message.id == id);
    const updatedMessage = { ...messages[messageIndex], ...message };
    messages[messageIndex] = updatedMessage;
    this.storage.writeItems(messages);
    return updatedMessage;
  }

  updateMessagesUser(user) {
    const messages = this.storage.readItems();
    const updatedMessages = messages.map((message) => {
      if (message.userId == user.id) {
        message.user = user.name;
        return message;
      }
      return message;
    });
    this.storage.writeItems(updatedMessages);
    return updatedMessages;
  }

  deleteMessage(id) {
    let messages = this.storage.readItems();
    const deletedMessage = messages.find((message) => message.id == id);
    messages = messages.filter((item) => item.id != id);
    this.storage.writeItems(messages);
    return deletedMessage;
  }
}

export default MessageService;
