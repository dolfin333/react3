import fs from "fs";
class JsonService {
  constructor(filePath) {
    this.filePath = filePath;
  }

  get nextId() {
    return this.readAll().nextId;
  }

  incrementNextId() {
    const jsonAll = this.readAll();
    jsonAll.nextId = jsonAll.nextId + 1;
    this.writeId(jsonAll.nextId);
    return jsonAll.nextId;
  }

  readItems() {
    const jsonText = fs.readFileSync(this.filePath);
    const jsonArray = JSON.parse(jsonText).items;
    return jsonArray;
  }

  readAll() {
    const jsonText = fs.readFileSync(this.filePath);
    const jsonAll = JSON.parse(jsonText);
    return jsonAll;
  }

  writeId(id) {
    const file = this.readAll();
    file.nextId = id;
    fs.writeFileSync(this.filePath, JSON.stringify(file, null, 4));
  }

  writeItems(items) {
    const file = this.readAll();
    file.items = items;
    fs.writeFileSync(this.filePath, JSON.stringify(file, null, 4));
  }
}

export default JsonService;
