import JsonService from "../json.service.js";
import userModel from "../../models/user.js";

class UserService {
  constructor(filePath) {
    this.storage = new JsonService(filePath);
  }

  getUsers() {
    const users = this.storage.readItems();
    return users;
  }

  getUserById(id) {
    const users = this.storage.readItems();
    const user = users.find((user) => user.id == id);
    return user;
  }

  getUserByLoginAndPassword({ login, password }) {
    const users = this.storage.readItems();
    const user = users.find(
      (user) => user.login == login && user.password == password
    );
    if (user) {
      return user;
    }
    return null;
  }

  addUser(user) {
    const users = this.storage.readItems();
    user.id = this.storage.nextId.toString();
    users.push({ ...userModel, ...user });
    this.storage.writeItems(users);
    this.storage.incrementNextId();
    return user;
  }

  updateUser(id, user) {
    const users = this.storage.readItems();
    const userIndex = users.findIndex((user) => user.id == id);
    const updatedUser = { ...users[userIndex], ...user };
    users[userIndex] = updatedUser;
    this.storage.writeItems(users);
    return updatedUser;
  }

  deleteUser(id) {
    let users = this.storage.readItems();
    const deletedUser = users.find((user) => user.id == id);
    users = users.filter((item) => item.id != id);
    this.storage.writeItems(users);
    return deletedUser;
  }
}

export default UserService;
